Spawns a reverse shell connecting to the given remote IP and port.

You need to change the `RHOST` and `RPORT` parameters inside the source code (`rev.c`).

**Note**: The `RHOST` parameter can be a domain name as well.

## How To Run
Compile with the following command-line:
```sh
make rev
```
This executable needs to be **on the target** machine.

Now, open a listening connection on your host machine. `netcat` to the rescue
here:
```sh
nc -lvp <RPORT>
```

Make sure your IP is accessible to the target machine. This won't be a problem
if both the machines are on the same network. If not, your host machine will
need to have a public IP somehow since the target will be connecting to your
machine for the reverse shell connection to work. Have a look at tools like
[ngrok](https://ngrok.com/).

Now run the executable on the target, and you should see a connection.

Have fun!
